# Search API EntityReferenceSelection

This module provides a selection plugin for entity reference autocomplete fields that lets you use Search API views.

- Add an Entity Reference Search API display to a Search API view.
- As with a normal Entity Reference view select the fields to search on
- Select the field that contains the ID of the entity
- Add a filter on Datasource that filters on the entity type that is used in the entity reference field
- In the settings of the entity reference field select the "Filter by an entity reference sapi view"

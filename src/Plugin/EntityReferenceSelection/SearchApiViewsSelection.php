<?php

namespace Drupal\sapi_ers\Plugin\EntityReferenceSelection;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\views\Plugin\EntityReferenceSelection\ViewsSelection;
use Drupal\views\Views;

/**
 * Provides an entity reference selection for search api views.
 *
 * @EntityReferenceSelection(
 *   id = "sapi_ers",
 *   label = @Translation("Views: Filter by an entity reference sapi view"),
 *   group = "sapi_ers",
 *   weight = 5
 * )
 */
class SearchApiViewsSelection extends ViewsSelection {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    // Remove the error that is triggered in parent form because no eligible
    // view is found.
    unset($form['no_view_help']);

    $view_settings = $this->getConfiguration()['view'];
    $displays = Views::getApplicableViews('entity_reference_display_sapi');
    // Filter views that list the entity type we want, and group the separate
    // displays by view.
    $view_storage = $this->entityTypeManager->getStorage('view');
    $entity_type = $this->entityTypeManager->getDefinition($this->configuration['target_type'])->id();

    $options = [];
    foreach ($displays as $data) {
      [$view_id, $display_id] = $data;
      $view_executable = $view_storage->load($view_id)->getExecutable();
      $view_executable->setDisplay($display_id);
      $display = $view_executable->getDisplay();
      $filters = $display->getOption('filters');
      // Filter views that do not filter on entity type.
      if (!empty($filters['search_api_datasource']) && (in_array('entity:' . $entity_type, $filters['search_api_datasource']['value']))) {
        $options[$view_id . ':' . $display_id] = $view_id . ' - ' . $display->pluginTitle();
      }
    }

    // The value of the 'view_and_display' select below will need to be split
    // into 'view_name' and 'view_display' in the final submitted values, so
    // we massage the data at validate time on the wrapping element (not
    // ideal).
    $form['view']['#element_validate'] = [
      [
        static::class,
        'settingsFormValidate',
      ],
    ];

    if ($options) {
      $default = !empty($view_settings['view_name']) ? $view_settings['view_name'] . ':' . $view_settings['display_name'] : NULL;
      $form['view']['view_and_display'] = [
        '#type' => 'select',
        '#title' => $this->t('View used to select the entities'),
        '#required' => TRUE,
        '#options' => $options,
        '#default_value' => $default,
        '#description' => '<p>' . $this->t('Choose the view and display that select the entities that can be referenced.<br />Only views with a display of type "Entity Reference Search API" are eligible.') . '</p>',
      ];

      $default = !empty($view_settings['arguments']) ? implode(', ', $view_settings['arguments']) : '';
      $form['view']['arguments'] = [
        '#type' => 'textfield',
        '#title' => $this->t('View arguments'),
        '#default_value' => $default,
        '#required' => FALSE,
        '#description' => $this->t('Provide a comma separated list of arguments to pass to the view.'),
      ];
    } else {
      if ($this->currentUser->hasPermission('administer views') && $this->moduleHandler->moduleExists('views_ui')) {
        $form['view']['no_view_help'] = [
          '#markup' => '<p>' . $this->t('No eligible views were found. <a href=":create">Create a view</a> with an <em>Entity Reference Search API</em> display, or add such a display to an <a href=":existing">existing view</a>.', [
              ':create' => Url::fromRoute('views_ui.add')->toString(),
              ':existing' => Url::fromRoute('entity.view.collection')
                ->toString(),
            ]) . '</p>',
        ];
      }
      else {
        $form['view']['no_view_help']['#markup'] = '<p>' . $this->t('No eligible views were found.') . '</p>';
      }
    }
    return $form;
  }

}

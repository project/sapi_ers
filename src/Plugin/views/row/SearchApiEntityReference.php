<?php

namespace Drupal\sapi_ers\Plugin\views\row;

use Drupal\views\Plugin\views\row\EntityReference;

/**
 * EntityReference row plugin.
 *
 * Used to render the row the same as an entity reference row. We can not use
 * that class, because it is limited to the entity_reference display type.
 *
 * @ingroup views_row_plugins
 *
 * @ViewsRow(
 *   id = "sapi_ers",
 *   title = @Translation("Entity Reference inline fields"),
 *   help = @Translation("Displays the fields with an optional template."),
 *   theme = "views_view_fields",
 *   register_theme = FALSE,
 *   display_types = {"sapi_ers"}
 * )
 */
class SearchApiEntityReference extends EntityReference {
}

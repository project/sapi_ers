<?php

namespace Drupal\sapi_ers\Plugin\views\display;

use Drupal\views\Plugin\views\display\DisplayPluginBase;

/**
 * The plugin that handles an EntityReference display for Search API.
 *
 * "entity_reference_display_sapi" is a custom property, used with
 * \Drupal\views\Views::getApplicableViews() to retrieve all views with an
 * 'Entity Reference Search API' display.
 *
 * @ingroup views_display_plugins
 *
 * @ViewsDisplay(
 *   id = "sapi_ers",
 *   title = @Translation("Entity Reference Search API"),
 *   admin = @Translation("Entity Reference Search API Source"),
 *   help = @Translation("Selects referenceable entities for an entity reference field."),
 *   theme = "views_view",
 *   register_theme = FALSE,
 *   uses_menu_links = FALSE,
 *   entity_reference_display_sapi = TRUE
 * )
 */
class SearchApiEntityReference extends DisplayPluginBase {

  /**
   * {@inheritdoc}
   */
  protected $usesAJAX = FALSE;

  /**
   * {@inheritdoc}
   */
  protected $usesPager = FALSE;

  /**
   * {@inheritdoc}
   */
  protected $usesAttachments = FALSE;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();

    // Force the style plugin to 'entity_reference_sapi_style' and the row plugin to
    // 'fields'.
    $options['style']['contains']['type'] = ['default' => 'sapi_ers'];
    $options['defaults']['default']['style'] = FALSE;
    $options['row']['contains']['type'] = ['default' => 'sapi_ers'];
    $options['defaults']['default']['row'] = FALSE;

    // Set the display title to an empty string (not used in this display type).
    $options['title']['default'] = '';
    $options['defaults']['default']['title'] = FALSE;

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function optionsSummary(&$categories, &$options) {
    parent::optionsSummary($categories, $options);
    // Disable 'title' so it won't be changed from the default set in
    // \Drupal\views\Plugin\views\display\EntityReference::defineOptions.
    unset($options['title']);
  }

  /**
   * {@inheritdoc}
   */
  public function getType(): string {
    return 'entity_reference_sapi';
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    return $this->view->render($this->display['id']);
  }

  /**
   * Builds the view result as a renderable array.
   *
   * @return array
   *   Renderable array or empty array.
   */
  public function render(): array {
    if (!empty($this->view->result) && $this->view->style_plugin->evenEmpty()) {
      return $this->view->style_plugin->render($this->view->result);
    }
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function usesExposed(): bool {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    if (!empty($this->view->live_preview)) {
      return;
    }

    $options = $this->getOption('entity_reference_options');

    // Restrict the autocomplete options based on what's been typed already.
    if (isset($options['match'])) {
      $style_options = $this->getOption('style');

      // Multiple search fields are ORed together.
      $conditions = $this->view->query->createConditionGroup('OR');

      // Build the condition using the selected search fields.
      foreach ($style_options['options']['search_fields'] as $field_id) {
        if (!empty($field_id)) {
          // Add an OR condition for the field.
          $conditions->addCondition($field_id, $options['match'], '=', 0);
        }
      }
      $this->view->query->addConditionGroup($conditions);
    }

    // Add an IN condition for validation.
   if (!empty($options['ids'])) {
     $id_field = preg_replace('/_[0-9]*$/', '', $this->view->getStyle()->options['id_field']);
     $this->view->query->addCondition($id_field, $options['ids'], 'IN');
   }

    $this->view->setItemsPerPage($options['limit']);
  }

  /**
   * {@inheritdoc}
   */
  public function validate(): array {
    $errors = parent::validate();
    // Verify that search fields and id field are set up.
    $style = $this->getOption('style');
    if (!isset($style['options']['search_fields'])) {
      $errors[] = $this->t('Display "@display" needs a selected search fields to work properly. See the settings for the Entity Reference list format.', ['@display' => $this->display['display_title']]);
    }
    else {
      // Verify that the search fields used actually exist.
      $fields = array_keys($this->handlers['field']);
      foreach ($style['options']['search_fields'] as $field_alias => $enabled) {
        if ($enabled && !in_array($field_alias, $fields)) {
          $errors[] = $this->t('Display "@display" uses field %field as search field, but the field is no longer present. See the settings for the Entity Reference list format.', ['@display' => $this->display['display_title'], '%field' => $field_alias]);
        }
      }
    }
    if (!isset($style['options']['id_field'])) {
      $errors[] = $this->t('Display "@display" needs a selected id field to work properly. See the settings for the Entity Reference list format.', ['@display' => $this->display['display_title']]);
    }
    else {
      // Verify that the search fields used actually exist.
      $fields = array_keys($this->handlers['field']);
      if (!in_array($style['options']['id_field'], $fields)) {
        $errors[] = $this->t('Display "@display" uses field %field as id field, but the field is no longer present. See the settings for the Entity Reference list format.', ['@display' => $this->display['display_title'], '%field' => $field_alias]);
      }
    }
    return $errors;
  }

}

<?php

namespace Drupal\sapi_ers\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Xss;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * EntityReference style plugin.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "sapi_ers",
 *   title = @Translation("Entity Reference list from Search API"),
 *   help = @Translation("Returns results as a PHP array of labels and rendered rows."),
 *   theme = "views_view_unformatted",
 *   register_theme = FALSE,
 *   display_types = {"sapi_ers"}
 * )
 */
class SearchApiEntityReference extends StylePluginBase {

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * {@inheritdoc}
   */
  protected $usesFields = TRUE;

  /**
   * {@inheritdoc}
   */
  protected $usesGrouping = FALSE;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();
    $options['search_fields'] = ['default' => []];
    $options['id_field'] = array('default' => '');

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $options = $this->displayHandler->getFieldLabels(TRUE);
    $form['search_fields'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Search fields'),
      '#options' => $options,
      '#required' => TRUE,
      '#default_value' => $this->options['search_fields'],
      '#description' => $this->t('Select the field(s) that will be searched when using the autocomplete widget.'),
      '#weight' => -3,
    ];
    $form['id_field'] = [
      '#type' => 'radios',
      '#title' => $this->t('ID field'),
      '#options' => $options,
      '#required' => TRUE,
      '#default_value' => $this->options['id_field'],
      '#description' => $this->t('Select the field that will be used for the ID of the reference.'),
      '#weight' => -3,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render(): array {
    if (!empty($this->view->live_preview)) {
      return parent::render();
    }

    // Group the rows according to the grouping field, if specified.
    $sets = $this->renderGrouping($this->view->result, $this->options['grouping']);

    // @todo We don't display grouping info for now. Could be useful for select
    // widget, though.
    $results = [];
    foreach ($sets as $records) {
      foreach ($records as $values) {
        $results[$values->_entity->id()] = $this->view->rowPlugin->render($values);
        // Sanitize HTML, remove line breaks and extra whitespace.
        $results[$values->_entity->id()]['#post_render'][] = function ($html, array $elements) {
          return Xss::filterAdmin(preg_replace('/\s\s+/', ' ', str_replace("\n", '', $html)));
        };
      }
    }
    return $results;
  }

  /**
   * {@inheritdoc}
   */
  public function evenEmpty(): bool {
    return TRUE;
  }

}
